import unittest

from editor import Image

class TestImageMethods(unittest.TestCase):
    # WANT: Add more test cases, especially negative tests.

    def test_create_image(self):
        img = Image(3, 4)
        self.assertEqual(
            [['O', 'O', 'O'], ['O', 'O', 'O'], ['O', 'O', 'O'], ['O', 'O', 'O']], \
            img._image)

    def test_color_image(self):
        img = Image(3, 4)
        img.color(1, 2, 'X')
        self.assertEqual(
            [['O', 'X', 'O'], ['O', 'O', 'O'], ['O', 'O', 'O'], ['O', 'O', 'O']], \
            img._image)

    def test_clear_image(self):
        img = Image(3, 4)
        img.color(1, 2, 'X')
        img = img.clear()
        self.assertEqual(
            [['O', 'O', 'O'], ['O', 'O', 'O'], ['O', 'O', 'O'], ['O', 'O', 'O']], \
            img._image)

    def test_draw_h_image(self):
        img = Image(3, 4)
        img.draw_h(2, 1, 3, 'X')
        self.assertEqual(
            [['O', 'O', 'O'], ['X', 'X', 'X'], ['O', 'O', 'O'], ['O', 'O', 'O']], \
            img._image)

    def test_draw_v_image(self):
        img = Image(3, 4)
        img.draw_v(1, 3, 2, 'X')
        self.assertEqual(
            [['O', 'X', 'O'], ['O', 'X', 'O'], ['O', 'X', 'O'], ['O', 'O', 'O']], \
            img._image)

    def test_fill_image(self):
        img = Image(3, 4)
        img.draw_v(1, 3, 2, 'X')
        img.draw_h(3, 2, 3, 'X')
        img.fill(2, 3, 'Y')
        self.assertEqual(
            [['O', 'X', 'Y'], ['O', 'X', 'Y'], ['O', 'X', 'X'], ['O', 'O', 'O']], \
            img._image)

    def test_fill_image_vertical_segment(self):
        img = Image(3, 4)
        img.draw_v(1, 3, 2, 'X')
        img.fill(2, 3, 'Y')
        self.assertEqual(
            [['Y', 'X', 'Y'], ['Y', 'X', 'Y'], ['Y', 'X', 'Y'], ['Y', 'Y', 'Y']], \
            img._image)

    def test_fill_image_diagonal_region(self):
        img = Image(3, 4)
        img.draw_h(1, 1, 3, 'X')
        img.color(2, 1, 'X')
        img.color(2, 2, 'X')
        img.color(3, 1, 'X')
        img.fill(2, 3, 'Y')
        self.assertEqual(
            [['X', 'X', 'X'], ['X', 'X', 'Y'], ['X', 'Y', 'Y'], ['Y', 'Y', 'Y']], \
            img._image)



if __name__ == '__main__':
    unittest.main()
