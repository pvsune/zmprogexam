from editor import Image

def create(match, img):
    x = int(match.group('x'))
    y = int(match.group('y'))

    if x >= 1 and y <= 250:
        return Image(x, y)

    return img


def clear(match, img):
    return img.clear()


def color(match, img):
    x = int(match.group('x'))
    y = int(match.group('y'))
    color = match.group('color')

    img.color(x, y, color)
    return img


def show(match, img):
    img.show()
    return img


def draw_v(match, img):
    x1 = int(match.group('x1'))
    x2 = int(match.group('x2'))
    y = int(match.group('y'))
    color = match.group('color')

    img.draw_v(x1, x2, y, color)
    return img


def draw_h(match, img):
    x = int(match.group('x'))
    y1 = int(match.group('y1'))
    y2 = int(match.group('y2'))
    color = match.group('color')

    img.draw_h(x, y1, y2, color)
    return img


def fill(match, img):
    x = int(match.group('x'))
    y = int(match.group('y'))
    color = match.group('color')

    img.fill(x, y, color)
    return img
