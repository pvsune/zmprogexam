from __future__ import print_function


class Image(object):
    def __init__(self, x=0, y=0):
        """
        @params:
            x - row count
            y - column count
        @returns:
            _x - row private attribute
            _y - column private attribute
            _image - image array private attribute

        Create Image() object.
        Make sure parameters are cleaned before instantiating."""

        self._image = self._create(x, y)
        self._x = x
        self._y = y

    def _create(self, x, y):
        yctr = 0
        xctr = 0
        image = list()

        while yctr < y:
            row = list()

            while xctr < x:
                row.append('O')
                xctr += 1

            yctr += 1
            xctr = 0
            image.append(row)

        return image


    def show(self):
        """
        @params: None
        @returns: None

        Prints the image.
        """

        for row in self._image:
            for pixel in row:
                print(pixel, end='')
            print()


    def clear(self):
        """
        @params: None
        @returns:
            New Image() object, all pixels cleared.
        """

        # TODO: check if recursive class instantiation is bad
        return Image(self._x, self._y)


    def color(self, x, y, color):
        """
        @params:
            x - row index (starting index 1)
            y - column index (starting index 1)
            color - Uppercase letter to replace the pixel
        @returns: None

        Changes the pixel by target color.
        """

        # normalize index
        _x = x-1
        _y = y-1

        # prohibit negative index
        if _x < 0 or _y < 0:
            return

        try:
            self._image[_x][_y] = color
        except IndexError:
            return


    def draw_v(self, x1, x2, y, color):
        """
        @params:
            x1 - target row start (starting index 1)
            x2 - target row end, inclusive (starting index 1)
            y - target column (starting index 1)
            color - Uppercase letter to replace the pixel
        @returns: None

        Draw vertical segment to image.
        """

        for each in range(x1, x2+1):
            self.color(each, y, color)


    def draw_h(self, x, y1, y2, color):
        """
        @params:
            x - target row (starting from 1)
            y1 - target column start (starting index 1)
            y2 - target column end, inclusive (starting index 1)
            color - Uppercase letter to replace the pixel
        @returns: None

        Draw horizontal segment to image.
        """

        for each in range(y1, y2+1):
            self.color(x, each, color)


    def fill(self, x, y, color):
        """
        @params:
            x - row index (starting index 1)
            y - column index (starting index 1)
            color - Uppercase letter to replace the pixel
        @returns: None

        Fill a region on the image using Flood-fill algorithm.
        """

        # prohibit values lt 1
        if x < 1 or y < 1:
            return

        try:
            pixel = self._image[x-1][y-1]
        except IndexError:
            return

        if pixel != 'O':
            return

        self._image[x-1][y-1] = color
        self.fill(x, y+1, color)
        self.fill(x, y-1, color)
        self.fill(x-1, y, color)
        self.fill(x+1, y, color)
