import re

from editor import Image
from edit import create, clear, color, show, draw_v, draw_h, fill

cmd = str()
img = Image()

while not re.match(u'X$', cmd):
    cmd = raw_input()
    regex = {
        u'I\s(?P<x>\d+)\s(?P<y>\d+)$': create,
        u'C$': clear,
        u'L\s(?P<y>\d+)\s(?P<x>\d+)\s(?P<color>[A-Z]+)$': color,
        u'S$': show,
        u'V\s(?P<y>\d+)\s(?P<x1>\d+)\s(?P<x2>\d+)\s(?P<color>[A-Z]+)$': draw_v,
        u'H\s(?P<y1>\d+)\s(?P<y2>\d+)\s(?P<x>\d+)\s(?P<color>[A-Z]+)$': draw_h,
        u'F\s(?P<x>\d+)\s(?P<y>\d+)\s(?P<color>[A-Z]+)$': fill }

    for each in regex.keys():
        match = re.match(each, cmd)

        if match:
            sub = regex[each]
            img = sub(match, img)
            break;
