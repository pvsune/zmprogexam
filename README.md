# Graphical Editor Simulation (ZMPROGEXAM)
ZipMatch Take-Home Exam

More details [here](https://dl.dropboxusercontent.com/u/81721171/Zipmatch-TechTest_2016.pdf)

# Synopsis
```bash
$ python edit.py # Run editor
$ python editor_test.py # Run tests
```

# How-To-Run
Install Python on your machine. Code is tested using `Python V2.7.12`.
Run `python edit.py` on your command line to start the editor.


# TODOs
- [ ] More test scenarios.
